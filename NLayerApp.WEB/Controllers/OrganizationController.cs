﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayerApp.BLL.Services;
using NLayerApp.BLL.Interfaces;
using NLayerApp.WEB.Models;
using NLayerApp.WEB.Mappers;

namespace NLayerApp.WEB.Controllers
{
    public class OrganizationController : Controller
    {
        private OrganizationService _organizationService;

        public OrganizationController()
        {
            _organizationService = new OrganizationService();
        }

        // GET: Organization
        public ActionResult Index()
        {
            IEnumerable<OrganizationViewModel> organizationViewModels = _organizationService
                .GetOrganizations()
                .Select(o => o.ToWebModel());
            return View(organizationViewModels);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(OrganizationViewModel organizationViewModel)
        {
            organizationViewModel.OrganizationSize = 0;
            _organizationService.CreateOrganization(organizationViewModel.ToBllModel());
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            _organizationService.DeleteOrganization(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            OrganizationViewModel organization = _organizationService
                .GetOrganizations()
                .Select(o => o.ToWebModel())
                .FirstOrDefault(o => o.Id == id);
            return View(organization);
        }
        [HttpPost]
        public ActionResult Edit(OrganizationViewModel organizationViewModel)
        {
            _organizationService.UpdateOrganization(organizationViewModel.ToBllModel());
            return RedirectToAction("Index");
        }

    }
}