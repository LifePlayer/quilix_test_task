﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayerApp.BLL.Services;
using NLayerApp.BLL.Interfaces;
using NLayerApp.WEB.Models;
using NLayerApp.WEB.Mappers;

namespace NLayerApp.WEB.Controllers
{
    public class EmployeeController : Controller
    {
        
        private EmployeeService _employeeService;

        public EmployeeController()
        {
            _employeeService = new EmployeeService();
        }

        public ActionResult Index()
        {
            IEnumerable<EmployeeViewModel> employeeViewModels = _employeeService
                .GetEmployees()
                .Select(e => e.ToWebModel());
            return View(employeeViewModels);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(EmployeeViewModel employeeViewModel)
        {
            employeeViewModel.AppliedDate = DateTime.Now;
            _employeeService.CreateEmployee(employeeViewModel.ToBllModel());
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            _employeeService.DeleteEmployee(id);
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public ActionResult Edit(int id)
        {
            IEnumerable<EmployeeViewModel> employeeViewModels = _employeeService
                .GetEmployees()
                .Select(e => e.ToWebModel());
            EmployeeViewModel employee = employeeViewModels.FirstOrDefault(e => e.Id == id);
            return View(employee);
        }
        [HttpPost]
        public ActionResult Edit(EmployeeViewModel employeeViewModel)
        {
            _employeeService.UpdateEmployee(employeeViewModel.ToBllModel());
            return RedirectToAction("Index");
        }

    }
}