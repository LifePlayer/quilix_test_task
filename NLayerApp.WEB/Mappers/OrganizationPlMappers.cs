﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLayerApp.BLL.DTO;
using NLayerApp.WEB.Models;

namespace NLayerApp.WEB.Mappers
{
    public static class OrganizationPlMappers
    {
        public static OrganizationViewModel ToWebModel(this OrganizationDTO organizationDto)
        {
            OrganizationViewModel result = new OrganizationViewModel()
            {
                Id = organizationDto.Id,
                Name = organizationDto.Name,
                OrganizationSize = organizationDto.OrganizationSize,
                LegalForm = organizationDto.LegalForm
            };
            return result;
        }

        public static OrganizationDTO ToBllModel(this OrganizationViewModel organization)
        {
            OrganizationDTO result = new OrganizationDTO()
            {
                Id = organization.Id,
                Name = organization.Name,
                OrganizationSize = organization.OrganizationSize,
                LegalForm = organization.LegalForm
            };
            return result;
        }

    }
}