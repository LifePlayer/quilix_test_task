﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLayerApp.BLL.DTO;
using NLayerApp.WEB.Models;

namespace NLayerApp.WEB.Mappers
{
    public static class EmployeePlMappers
    {
        public static EmployeeViewModel ToWebModel(this EmployeeDTO employeeDto)
        {
            EmployeeViewModel result = new EmployeeViewModel()
            {
                Id = employeeDto.Id,
                FirstName = employeeDto.FirstName,
                SecondName = employeeDto.SecondName,
                MiddleName = employeeDto.MiddleName,
                AppliedDate = employeeDto.AppliedDate,
                Position = employeeDto.Position,
                OrganizationId = employeeDto.OrganizationId
            };
            return result;
        }

        public static EmployeeDTO ToBllModel(this EmployeeViewModel employee)
        {
            EmployeeDTO result = new EmployeeDTO()
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                SecondName = employee.SecondName,
                MiddleName = employee.MiddleName,
                AppliedDate = employee.AppliedDate,
                Position = employee.Position,
                OrganizationId = employee.OrganizationId
            };
            return result;
        }

    }
}