﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NLayerApp.WEB.Models
{
    public class OrganizationViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int OrganizationSize { get; set; }
        [Required]
        public string LegalForm { get; set; }
    }
}