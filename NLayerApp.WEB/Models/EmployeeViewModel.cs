﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NLayerApp.WEB.Models
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string SecondName { get; set; }
        [Required]
        public string MiddleName { get; set; }
        public DateTime AppliedDate { get; set; }
        [Required]
        public string Position { get; set; }

        public int OrganizationId { get; set; }
    }
}