﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.DAL.Entities;

namespace NLayerApp.DAL.Entities
{
    public class Organization
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int OrganizationSize { get; set; }
        public string LegalForm { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}
