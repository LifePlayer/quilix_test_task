﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.DAL.Entities;
using System.Data.Common;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace NLayerApp.DAL.Repositories
{
    public abstract class BaseRepository<TModel> where TModel : class
    {
        protected string _tableName;

        public BaseRepository(string tableName)
        {
            _tableName = tableName;
        }

        public IEnumerable<TModel> GetAll()
        {
            List<TModel> result = new List<TModel>();

            //string provider = ConfigurationManager.ConnectionStrings["provider"].ConnectionString;
            //string db = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
            string provider = "System.Data.SqlClient";
            string db = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\EmployeeRegistration.mdf';Integrated Security=True";

            DbProviderFactory dataFactory = DbProviderFactories.GetFactory(provider);

            using (DbConnection connection = dataFactory.CreateConnection())
            {
                connection.ConnectionString = db;
                connection.Open();

                DbCommand cmd = dataFactory.CreateCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * From " + _tableName;

                using (DbDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        result.Add(this.MapModel(dataReader));
                    }
                }
                cmd.Dispose();
            }
            return result;
        }

        public void Delete(int id)
        {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\EmployeeRegistration.mdf';Integrated Security=True";
            string query = string.Format("DELETE FROM {0} WHERE Id = '{1}'", _tableName, id);

            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                connection.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Exception error = new Exception("Error", ex);
                }
                connection.Close();
            }
        }

        public abstract TModel MapModel(DbDataReader dataReader);

    }
}
