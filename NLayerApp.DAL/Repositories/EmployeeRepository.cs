﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.DAL.Entities;
using NLayerApp.DAL.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace NLayerApp.DAL.Repositories
{
    public class EmployeeRepository : BaseRepository<Employee>, IRepository<Employee>
    {
        public EmployeeRepository() : base("Employees") { }

        public override Employee MapModel(DbDataReader dataReader)
        {
            Employee model = new Employee()
            {
                Id = (int)dataReader["Id"],
                FirstName = (string)dataReader["FirstName"],
                SecondName = (string)dataReader["SecondName"],
                MiddleName = (string)dataReader["MiddleName"],
                AppliedDate = (DateTime)dataReader["AppliedDate"],
                Position = (string)dataReader["Position"],
                OrganizationId = (int)dataReader["OrganizationId"]
            };

            return model;
        }

        public void Insert(Employee model)
        {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\EmployeeRegistration.mdf';Integrated Security=True";
            string query = "INSERT INTO " + this._tableName + " (FirstName, SecondName, MiddleName, AppliedDate, Position, OrganizationId) " +
                "VALUES (@FirstName, @SecondName, @MiddleName, @AppliedDate, @Position, @OrganizationId) ";

            using (SqlConnection cn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                cmd.Parameters.Add("@FirstName", SqlDbType.NChar, 50).Value = model.FirstName;
                cmd.Parameters.Add("@SecondName", SqlDbType.NChar, 50).Value = model.SecondName;
                cmd.Parameters.Add("@MiddleName", SqlDbType.NChar, 50).Value = model.MiddleName;
                cmd.Parameters.Add("@AppliedDate", SqlDbType.Date).Value = model.AppliedDate;
                cmd.Parameters.Add("@Position", SqlDbType.NChar, 50).Value = model.Position;
                cmd.Parameters.Add("@OrganizationId", SqlDbType.Int).Value = model.OrganizationId;

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public void Update(Employee model)
        {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\EmployeeRegistration.mdf';Integrated Security=True";
            string query = string.Format("UPDATE Employees " +
                "SET FirstName = @FirstName, SecondName = @SecondName, MiddleName = @MiddleName, " +
                "Position = @Position, OrganizationId = @OrganizationId " +
                "WHERE Id = @Id");
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.Add("@FirstName", SqlDbType.NChar, 50).Value = model.FirstName;
                cmd.Parameters.Add("@SecondName", SqlDbType.NChar, 50).Value = model.SecondName;
                cmd.Parameters.Add("@MiddleName", SqlDbType.NChar, 50).Value = model.MiddleName;
                cmd.Parameters.Add("@Position", SqlDbType.NChar, 50).Value = model.Position;
                cmd.Parameters.Add("@OrganizationId", SqlDbType.Int).Value = model.OrganizationId;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = model.Id;
                connection.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Exception error = new Exception("Error", ex);
                }
                connection.Close();
            }
        }

    }
}
