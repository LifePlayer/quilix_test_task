﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.DAL.Entities;
using NLayerApp.DAL.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace NLayerApp.DAL.Repositories
{
    public class OrganizationRepository : BaseRepository<Organization>, IRepository<Organization>
    {

        public OrganizationRepository() : base("Organizations") { }

        public override Organization MapModel(DbDataReader dataReader)
        {
            Organization model = new Organization()
            {
                Id = (int)dataReader["Id"],
                Name = (string)dataReader["Name"],
                OrganizationSize = (int)dataReader["OrganizationSize"],
                LegalForm = (string)dataReader["LegalForm"]
            };
            return model;
        }

        public void Insert(Organization model)
        {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\EmployeeRegistration.mdf';Integrated Security=True";
            string query = "INSERT INTO " + this._tableName + " (Name, OrganizationSize, LegalForm) " +
                "VALUES (@Name, @OrganizationSize, @LegalForm) ";

            using (SqlConnection cn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                cmd.Parameters.Add("@Name", SqlDbType.NChar, 50).Value = model.Name;
                cmd.Parameters.Add("@OrganizationSize", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@LegalForm", SqlDbType.NChar, 50).Value = model.LegalForm;

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public void Update(Organization model)
        {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\EmployeeRegistration.mdf';Integrated Security=True";
            string query = string.Format("UPDATE Organizations " +
                "SET Name = @Name, LegalForm = @LegalForm " + 
                "WHERE Id = @Id");
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.Add("@Name", SqlDbType.NChar, 50).Value = model.Name;
                cmd.Parameters.Add("@LegalForm", SqlDbType.NChar, 50).Value = model.LegalForm;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = model.Id;
                connection.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Exception error = new Exception("Error", ex);
                }
                connection.Close();
            }
        }
    }
}
