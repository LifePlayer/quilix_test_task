﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.BLL.DTO;
using NLayerApp.DAL.Entities;

namespace NLayerApp.BLL.Mappers
{
    public static class OrganizationMappers
    {
        public static OrganizationDTO ToBllModel(this Organization organization)
        {
            OrganizationDTO result = new OrganizationDTO()
            {
                Id = organization.Id,
                Name = organization.Name,
                OrganizationSize = organization.OrganizationSize,
                LegalForm = organization.LegalForm
            };
            return result;
        }

        public static Organization ToDalEntity(this OrganizationDTO organization)
        {
            Organization result = new Organization()
            {
                Id = organization.Id,
                Name = organization.Name,
                OrganizationSize = organization.OrganizationSize,
                LegalForm = organization.LegalForm
            };

            return result;
        }

    }
}
