﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.BLL.DTO;
using NLayerApp.DAL.Entities;

namespace NLayerApp.BLL.Mappers
{
    public static class EmployeeMappers
    {

        public static EmployeeDTO ToBllModel(this Employee employee)
        {
            EmployeeDTO result = new EmployeeDTO()
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                SecondName = employee.SecondName,
                MiddleName = employee.MiddleName,
                AppliedDate = employee.AppliedDate,
                Position = employee.Position,
                OrganizationId = employee.OrganizationId
            };

            return result;
        }

        public static Employee ToDalEntity(this EmployeeDTO employee)
        {
            Employee result = new Employee()
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                SecondName = employee.SecondName,
                MiddleName = employee.MiddleName,
                AppliedDate = employee.AppliedDate,
                Position = employee.Position,
                OrganizationId = employee.OrganizationId
            };

            return result;
        }

    }
}
