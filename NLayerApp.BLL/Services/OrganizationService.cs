﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.DAL.Interfaces;
using NLayerApp.DAL.Repositories;
using NLayerApp.DAL.Entities;
using NLayerApp.BLL.DTO;
using NLayerApp.BLL.Interfaces;
using NLayerApp.BLL.Mappers;

namespace NLayerApp.BLL.Services
{
    public class OrganizationService : IOrganizationService
    {
        private IRepository<Organization> _organizationRepo;

        public OrganizationService()
        {
            _organizationRepo = new OrganizationRepository();
        }

        public IEnumerable<OrganizationDTO> GetOrganizations()
        {
            IEnumerable<OrganizationDTO> organizations = _organizationRepo
                .GetAll()
                .Select(o => o.ToBllModel());
            return organizations;
        }

        public void CreateOrganization(OrganizationDTO organizationDto)
        {
            _organizationRepo.Insert(organizationDto.ToDalEntity());
        }

        public void DeleteOrganization(int id)
        {
            _organizationRepo.Delete(id);
        }

        public void UpdateOrganization(OrganizationDTO organizationDto)
        {
            _organizationRepo.Update(organizationDto.ToDalEntity());
        }

    }
}
