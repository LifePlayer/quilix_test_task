﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.DAL.Interfaces;
using NLayerApp.DAL.Repositories;
using NLayerApp.DAL.Entities;
using NLayerApp.BLL.DTO;
using NLayerApp.BLL.Interfaces;
using NLayerApp.BLL.Mappers;

namespace NLayerApp.BLL.Services
{
    public class EmployeeService : IEmployeeService
    {
        private IRepository<Employee> _employeeRepo;

        public EmployeeService()
        {
            _employeeRepo = new EmployeeRepository();
        }

        public IEnumerable<EmployeeDTO> GetEmployees()
        {
            IEnumerable<EmployeeDTO> employees = _employeeRepo
                .GetAll()
                .Select(e => e.ToBllModel());
            return employees;
        }

        public void CreateEmployee(EmployeeDTO employeeDto)
        {
            _employeeRepo.Insert(employeeDto.ToDalEntity());
        }

        public void DeleteEmployee(int id)
        {
            _employeeRepo.Delete(id);
        }

        public void UpdateEmployee(EmployeeDTO employeeDto)
        {
            _employeeRepo.Update(employeeDto.ToDalEntity());
        }

    }
}
