﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.BLL.DTO;

namespace NLayerApp.BLL.Interfaces
{
    public interface IEmployeeService
    {
        IEnumerable<EmployeeDTO> GetEmployees();
        void CreateEmployee(EmployeeDTO employeeDto);
        void DeleteEmployee(int id);
        void UpdateEmployee(EmployeeDTO employeeDto);
    }
}
