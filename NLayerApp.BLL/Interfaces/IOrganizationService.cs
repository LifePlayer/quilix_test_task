﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayerApp.BLL.DTO;

namespace NLayerApp.BLL.Interfaces
{
     public interface IOrganizationService
    {
        IEnumerable<OrganizationDTO> GetOrganizations();
        void CreateOrganization(OrganizationDTO organizationDto);
        void DeleteOrganization(int id);
        void UpdateOrganization(OrganizationDTO organizationDto);
    }
}
