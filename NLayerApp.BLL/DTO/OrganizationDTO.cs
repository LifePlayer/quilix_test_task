﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLayerApp.BLL.DTO
{
    public class OrganizationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int OrganizationSize { get; set; }
        public string LegalForm { get; set; }
    }
}
